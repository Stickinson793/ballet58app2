angular.module('starter.controllers', ['ngBalletService'])



    .controller('AppCtrl', function($scope, $ionicModal, $timeout, Teacher) {

        //var users = User.find({}, function(face){
        //    alert(users)
        //    alert(face)
        //});
        //console.log("user:", users)
        $scope.loginError = null;
        $scope.createError = null;
        // Form data for the login modal
        $scope.loginData = {
            email: null,
            password: null
        };
        $scope.teacherCreateData = {
            firstname: null,
            lastname: null,
            email: null,
            password: null,
            admin: "N",
            teacherid: 0
        };

        //check if we have a current teacher
        var teacherId = window.localStorage.getItem('teacherId');
        console.log(teacherId)
        if(teacherId){//s && $.isNumesrric(teacherId)){
            $scope.teacher = Teacher.findById({'id':teacherId},
                function(user){},
                function(error){
                    console.debug(error);
                    window.localStorage.removeItem('$LoopBack$accessTokenId');
                    window.localStorage.removeItem('$LoopBack$currentteacherId');
                    window.localStorage.removeItem('teacherId');
                }
            );
        }

        var saveTeacherInformation = function(){
            $scope.teacher = Teacher.create(
                {
                    firstname: $scope.teacherCreateData.firstname,
                    lastname: $scope.teacherCreateData.lastname,
                    email: $scope.teacherCreateData.email,
                    password: $scope.teacherCreateData.password,
                    admin: "N",
                    teacherid: $scope.teacherCreateData.teacherid
                },
                function (res) {
                    //console.log('res');
                    console.debug(res.teacherid);
                    if (res.teacherid) {
                        Teacher.login({rememberMe: $scope.rememberMe}, $scope.teacherCreateData, function (res) {
                            $scope.closeCreateDialog();
                            $scope.closeLogin();
                            $scope.teacher = res.user;

                            //set the teacher id in local storage
                            window.localStorage.setItem("teacherId", res.userId);
                        });

                        $scope.createError = null;
                    }
                    //TODO add error handling
                }, function (res) {
                    console.log('error');
                    console.log(res);
                    $scope.createError = res.data.error.message;
                });
        };

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.loginModal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function() {
            $scope.loginModal.hide();
        };

        // Open the login modal
        $scope.login = function() {
            $scope.loginModal.show();
        };

        // Perform the login action when the teacher submits the login form
        $scope.doLogin = function () {
            console.log('Doing login', $scope.loginData);

            Teacher.login({rememberMe: true}, $scope.loginData, function (res) {
                $scope.closeLogin();
                $scope.loginError = null;

                $scope.teacher = res.user;
                window.localStorage.setItem("teacherId", res.userId)
            }, function (res) {
                $scope.loginError = res.data.error.message;
                console.debug(res.data);
                //TODO add error handling
            });
        };

        /**
         * Manage teacher Creation
         */

            // Create the createTeacherModal that we will use later
        $ionicModal.fromTemplateUrl('templates/create-user.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.createTeacherModal = modal;
        });

        $scope.closeCreateDialog = function(){
            $scope.createTeacherModal.hide();
        };

        $scope.saveTeacher = function () {
            if($scope.teacherCreateData.email && $scope.teacherCreateData.password) {
                //do save of teacher info
                saveTeacherInformation();
            }
        };

        $scope.createTeacher = function () {
            $scope.createTeacherModal.show();
        };
    })

    .controller('DashCtrl', function($scope) {})

    .controller('ChatsCtrl', function($scope, Chats) {
        $scope.chats = Chats.all();
        $scope.remove = function(chat) {
            Chats.remove(chat);
        }
    })

    .controller('AttendanceCtrl', function($scope, $stateParams, Attendance) {
        $scope.attendances = Attendance.findOne(classid);

    })

    .controller('ClassesCtrl', function($scope, $stateParams, Class) {
       $scope.classes = Class.find({filter:{include:['course'],where:{teacherid:$scope.teacher.teacherid}}});
    })

    .controller('ClassDetailCtrl', function($scope, $ionicModal, $stateParams, Regist, Attendance) {
        $scope.regists = Regist.find({filter:{include:['student'],where:{classid:$stateParams.classId}}});

        $ionicModal.fromTemplateUrl('templates/comment.html', {
            scope: $scope
        }).then(function(commentModal) {
            $scope.commentModal = commentModal;
        });

        // Triggered in the comment modal to close it
        $scope.closeComment = function() {
            $scope.commentModal.hide();
        };

        // Open the comment modal
        $scope.comment = function() {
            $scope.commentModal.show();
        };


       /*$scope.doComment = function() {
            $scope.date = new Date();

            $scope.attendance = Attendance.find({filter:{date:date,registid: }})
        }*/


        /*$scope.attended = function(registid, date, status, note){
            Attendance.upsert(registid, this.date, status, note)

        }*/
    })

    /*.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
        $scope.chat = Chats.get($stateParams.chatId);
    })

    .controller('FriendsCtrl', function($scope, Friends) {
        $scope.friends = Friends.all();
    })

    .controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
        $scope.friend = Friends.get($stateParams.friendId);
    })

    .controller('AccountCtrl', function($scope) {
        $scope.settings = {
            enableFriends: true
        };
    })*/

    .controller('DashboardCtrl', function($scope) {
        $scope.settings = {
            enableFriends: false
        };
    });
